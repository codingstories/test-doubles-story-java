---
focus: /src/main/java/com/epam/engx/story/DefaultReservationService.java
---
### Test Doubles Story
Welcome to coding story about small hotel reservation service.
This story is the usage of test doubles in order to prepare well-written unit tests.

First let's take a look on project structure to understand what is this
business logic about. We define two interfaces of repositories that we will be used in the logic:
[HotelRepository](/src/main/java/com/epam/engx/story/HotelRepository.java), with one `findHotelsByCity` method,
and [ReservationRepository](/src/main/java/com/epam/engx/story/ReservationRepository.java), with two methods:
`isOpenForReservation` and `reserve`. They are interfaces only, as we don't care about their actual implementation.
The main class, that we will test is 
[DefaultReservationService](/src/main/java/com/epam/engx/story/DefaultReservationService.java),
where we define logic for performing reservation of the best hotel in the given city in defined date range.
