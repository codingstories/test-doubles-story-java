package com.epam.engx.story.model;

import java.math.BigInteger;
import java.util.Objects;

public class Hotel {

    private final long id;
    private final String name;
    private final String city;
    private final int rank;
    private final BigInteger pricePerNight;

    public Hotel(long id, String name, String city, int rank, BigInteger pricePerNight) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.rank = rank;
        this.pricePerNight = pricePerNight;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getRank() {
        return rank;
    }

    public BigInteger getPricePerNight() {
        return pricePerNight;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Hotel hotel = (Hotel) o;
        return id == hotel.id &&
                rank == hotel.rank &&
                Objects.equals(name, hotel.name) &&
                Objects.equals(city, hotel.city) &&
                Objects.equals(pricePerNight, hotel.pricePerNight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, city, rank, pricePerNight);
    }

    public static class Builder {
        private long id;
        private String name;
        private String city;
        private int rank;
        private BigInteger pricePerNight;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder rank(int rank) {
            this.rank = rank;
            return this;
        }

        public Builder pricePerNight(BigInteger pricePerNight) {
            this.pricePerNight = pricePerNight;
            return this;
        }

        public Hotel build() {
            return new Hotel(id, name, city, rank, pricePerNight);
        }
    }

}
