package com.epam.engx.story;

import java.util.Date;

public interface ReservationService {

    void reserveBestHotelInCity(String city, Date startDate, Date endDate);

}
