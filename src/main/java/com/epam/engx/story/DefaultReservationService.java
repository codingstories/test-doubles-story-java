package com.epam.engx.story;

import com.epam.engx.story.model.Hotel;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class DefaultReservationService implements ReservationService {

    private final HotelRepository hotelRepository;
    private final ReservationRepository reservationRepository;

    public DefaultReservationService(HotelRepository hotelRepository, ReservationRepository reservationRepository) {
        this.hotelRepository = hotelRepository;
        this.reservationRepository = reservationRepository;
    }

    @Override
    public void reserveBestHotelInCity(String city, Date startDate, Date endDate) {
        List<Hotel> allHotelsInCity = hotelRepository.findHotelsByCity(city);
        List<Hotel> sortedHotels = sortByRank(allHotelsInCity);
        for (Hotel hotel : sortedHotels) {
            boolean hotelOpenForReservation = reservationRepository.isOpenForReservation(hotel.getId(), startDate, endDate);
            if (hotelOpenForReservation) {
                reservationRepository.reserve(hotel.getId(), startDate, endDate);
                return;
            }
        }
    }

    private List<Hotel> sortByRank(List<Hotel> allHotelsInCity) {
        return allHotelsInCity.stream()
                .sorted(Comparator.comparingInt(Hotel::getRank))
                .collect(toList());
    }

}
