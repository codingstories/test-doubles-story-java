package com.epam.engx.story;

import java.util.Date;

public interface ReservationRepository {

    boolean isOpenForReservation(long hotelId, Date startDate, Date endDate);

    void reserve(long hotelId, Date startDate, Date endDate);

}
