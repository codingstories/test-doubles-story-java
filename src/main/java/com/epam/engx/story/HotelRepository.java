package com.epam.engx.story;

import com.epam.engx.story.model.Hotel;

import java.util.List;

public interface HotelRepository {

    List<Hotel> findHotelsByCity(String city);

}
