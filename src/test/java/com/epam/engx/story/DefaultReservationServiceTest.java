package com.epam.engx.story;

import com.epam.engx.story.model.Hotel;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class DefaultReservationServiceTest {

    private static final String CITY = "Paris";
    private static final Date START_DATE = Date.from(Instant.parse("2022-10-12T20:00:00.00Z"));
    private static final Date END_DATE = Date.from(Instant.parse("2022-10-13T10:00:00.00Z"));
    private static final long BEST_HOTEL_ID = 1L;
    private static final long OTHER_HOTEL_ID = 2L;

    @Test
    void shouldReserveBestHotelWhenAvailable() {
        // given
        HotelRepository hotelRepository = new HotelRepositoryStub();
        ReservationRepository reservationRepository = mock(ReservationRepository.class);
        DefaultReservationService reservationService = new DefaultReservationService(hotelRepository, reservationRepository);

        // when
        reservationService.reserveBestHotelInCity(CITY, START_DATE, END_DATE);
        when(reservationRepository.isOpenForReservation(BEST_HOTEL_ID, START_DATE, END_DATE)).thenReturn(false);
        when(reservationRepository.isOpenForReservation(OTHER_HOTEL_ID, START_DATE, END_DATE)).thenReturn(true);

        //then
        verify(reservationRepository, times(1)).reserve(OTHER_HOTEL_ID, START_DATE, END_DATE);
    }

    private static class HotelRepositoryStub implements HotelRepository {

        private final Hotel bestHotel = Hotel.builder()
                .id(BEST_HOTEL_ID)
                .rank(1)
                .build();

        private final Hotel otherHotel = Hotel.builder()
                .id(OTHER_HOTEL_ID)
                .rank(2)
                .build();

        @Override
        public List<Hotel> findHotelsByCity(String city) {
            return List.of(bestHotel, otherHotel);
        }
    }

}